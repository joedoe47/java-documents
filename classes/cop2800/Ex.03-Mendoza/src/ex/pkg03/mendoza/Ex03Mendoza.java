/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ex.pkg03.mendoza;

import java.util.ArrayList;

/**
 *
 * @author joedoe47
 */
public class Ex03Mendoza {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       
        ArrayList<Ship> myShip = new ArrayList<Ship>();

        
        myShip.add(new CargoShip("Oil Tanker",15));
        myShip.add(new CruiseShip("Royal Caribbean 1", 7));
        myShip.add(new CargoShip("Bulk Carrier",8));
        myShip.add(new CruiseShip("Carnical Cruise 7", 20));
        myShip.add(new CargoShip("Refrigerated Cargo Ship",25));
        myShip.add(new CruiseShip("Norwegian Cruise 4", 13));
        
        
        for(Ship vessle: myShip){
            System.out.println("Name: " + vessle.getName());
            System.out.println("Year: " + vessle.getYear());
            System.out.println("Max Capacity: " + vessle.getOutput());
            System.out.println("========\n");
        } 
        

    } }
