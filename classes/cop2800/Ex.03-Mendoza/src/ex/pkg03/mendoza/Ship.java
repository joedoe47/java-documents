/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ex.pkg03.mendoza;

/**
 *
 * @author joedoe47
 */
public class Ship {
    String name;
    int year;

public Ship(String name, int year){
    this.name = name;
    this.year = year;
    
}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getOutput() {
        String output;
        output=Integer.toString(year);
        return output;
    }

    
    
}
