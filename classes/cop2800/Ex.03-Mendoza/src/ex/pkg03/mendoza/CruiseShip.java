/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ex.pkg03.mendoza;

/**
 *
 * @author joedoe47
 */
public class CruiseShip extends Ship{
    int max_passengers = 5000;

    public CruiseShip(String name, int year) {
        super(name, year);
    }

    public int getMax_passengers() {
        return max_passengers;
    }

    public void setMax_passengers(int max_passengers) {
        this.max_passengers = max_passengers;
    }
    
    @Override
    public String getOutput() {
        String output;
        output=Integer.toString(max_passengers) + " People";
        return output;
    }
    
}
