/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ex.pkg03.mendoza;

/**
 *
 * @author joedoe47
 */
public class CargoShip extends Ship{
    int lbs_cap = 24000;

    public CargoShip(String name, int year) {
        super(name, year);
    }

    public int getLbs_cap() {
        return lbs_cap;
    }

    public void setLbs_cap(int lbs_cap) {
        this.lbs_cap = lbs_cap;
    }
    
    @Override
    public String getOutput() {
        String output;
        output=Integer.toString(lbs_cap) + " lbs";
        return output;
    }
    
}
