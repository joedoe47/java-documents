/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ex.pkg02.mendoza;

/**
 *
 * @author joedoe47
 */
public class view_seats {
        int seats [][] = {
            {10,10,20,20,20,20,20,20,10,10},
            {10,10,20,20,20,20,20,20,10,10},
            {20,20,30,30,40,40,30,30,20,20},
            {20,30,30,40,50,50,40,30,30,20},
            {30,40,50,50,50,50,50,50,40,30},
        };
    
    int finding;// this variable is the value that will be used to rerwrite a value
    int row; //these will help specify where to replace
    int col;

    
    public void setSeats(int seats [][]) {
        this.seats = seats;
    }

    public void setFinding( int row, int col) {
        this.row = row;
        this.col = col;

        seats[row][col] = 0;
    }
    
    public view_seats(){
        
    }

    public int getFinding() {
        return finding;
    }
    public void getSeats() {
        System.out.println("\nAvilable seats:\n====");
        for(int curRow = 0; curRow < seats.length ; curRow++){
            System.out.printf("Row %d |", curRow);
            for(int curCol=0; curCol < seats[curRow].length ; curCol++){
                
                System.out.printf("%d($%d) | ", curCol, seats[curRow][curCol] );
            }
            System.out.println("");
        }
        
        System.out.println("==== \n");
    }

    
    
    
}
