/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ex.pkg02.mendoza;

import java.util.Scanner;

/**
 *
 * @author joedoe47
 */
public class Ex02Mendoza {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //variable
        int choice=0;
        int price_range=0;
        int seatrow=0;
        int seatcol=0;
        int finding=0;

        
        // call
        view_seats list=new view_seats();
        buy_price by_price = new buy_price();
        buy_seat by_seat = new buy_seat();
        
        Scanner menu = new Scanner(System.in);
        Scanner price = new Scanner(System.in);
        //list.getSeats();
        //"UI" menu

        do {
                System.out.println("Welcome to professor Zejnilovic's theater.\n" 
                + "Please select which option you wish to do.\n\n" 
                + "please press '0' to quit\n\n"
                + "1) to buy a specific seat\n"
                + "2) to buy any seat by price\n"
                + "3) to view avilable seats\n"
                + "your choice:");
                choice = menu.nextInt();
                
                //act on user choice
            switch(choice){
            case 0:
                System.out.println("Thank you for coming!\n");
                break;
            case  1:
                System.out.println("please enter the row you want:");
                seatrow = price.nextInt(); //input
                System.out.println("Please enter the colum you wish to purchase:");
                seatcol = price.nextInt(); //input
                
                by_seat.setInput(seatrow,seatcol); //find the seat and price
                //Debug
                //System.out.println("you entered:");
                //System.out.println(by_seat.getInput());
                
                //set into array
                list.setFinding(seatrow,seatcol);

                break;
            case 2:
                //input price
                System.out.println("What price would you like?");
                price_range = price.nextInt();//input
                by_price.setInput(price_range); //find the first seat of price entered
                
                //debug
                //System.out.println("you entered:" + by_price.getInput());
                System.out.println(by_price.getFinding());
                //see row and col
                //System.out.println("here is the row: " + by_price.getRow()+ "\n" + "here is the col: " + by_price.getCol());
                
                //set the array
                list.setFinding(by_price.getRow(),by_price.getCol());
                
                break;
            case 3:
                System.out.println("Here is a list of all the seats prices.");
                list.getSeats();
                break;
            default:
                System.out.println("Please select 1,2,3, or 0\n\n");
        
        }
                
        } while(choice != 0);
    }
    
}
