/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ex.pkg02.mendoza;

/**
 *
 * @author joedoe47
 */
public class buy_price extends view_seats{
    int input;
    
    @Override
    public int getFinding() {
        int limit=0;
        //see through array for first match
        for(int curRow = 0; curRow < seats.length ; curRow++){
            for(int curCol=0; curCol < seats[curRow].length ; curCol++){
                finding = seats[curRow][curCol]; //temporarily add the array value(10) to int finding
                for(; limit <= 0 ; ){
                    if (finding == input){
                        System.out.printf("seat %d, in row %d, has been marked to be purchased for %d.\n", curCol, curRow, seats[curRow][curCol]);
                        row = curRow;
                        col = curCol;
                        limit++; // stop searching once 1 match is found
                        break;
                    }  
                    break;
                }
            }
            System.out.println("");
        }
        return finding;
    }
    
    public int getRow() {
       return  row;
    }
    
    public int getCol() {
        return col;
    }


    public void setInput(int input) {
        this.input = input;
    }
    
    public int getInput() {
        return input;
    }
    
}
